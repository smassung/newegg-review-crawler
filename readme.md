# Categories crawled

 - hdd: 97 items, 22416 reviews
 - monitor: 107 items, 12890 reviews
 - motherboard: 249 items, 23419 reviews
 - power-supply: 204 items, 33399 reviews
 - router: 191 items, 26978 reviews
 - video-card: 282 items, 17819 reviews

# Egg (rating) distribution:
 - 1: 19,514 (14.25%)
 - 2: 8,547 (6.24%)
 - 3: 8,912 (6.51%)
 - 4: 20,401 (14.9%)
 - 5: 79,547 (58.1%)

1,130 total items, 136,921 total reviews
